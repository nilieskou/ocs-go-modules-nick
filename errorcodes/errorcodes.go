package errorcodes

const (
	// KubeClient is the exit code when trivy-k8s-wrapper fails to initialize a kube client
	KubeClient = 3
	// FlagsValidation is the exit code when trivy-k8s-wrapper fails to validate the input flags
	FlagsValidation = 4
	// TrivyVersion is the exit code when trivy-k8s-wrapper fails to get the Trivy version
	TrivyVersion = 5
	// TrivyScan is the exit code when trivy-k8s-wrapper fails to make a Trivy scan
	TrivyScan = 6
	// SizeLimit is the exit code when trivy-k8s-wrapper fails because the Trivy report is bigger than the limit
	SizeLimit = 7
	// DataConvertion is the exit code when trivy-k8s-wrapper fails to get a data converter
	DataConvertion = 8
	// ToConsolidatedReport is the exit code when trivy-k8s-wrapper fails to convert the Trivy report to a consolidated report
	ToConsolidatedReport = 9
	// PrepareData is the exit code when trivy-k8s-wrapper fails to prepare the data for the configmaps
	PrepareData = 10
	// ChainedConfigmaps is the exit code when trivy-k8s-wrapper fails to create the chained configmaps
	ChainedConfigmaps = 11
)
