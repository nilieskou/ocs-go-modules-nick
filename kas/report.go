package kas

import (
	"time"
)

// ConsolidatedReport Type referenced from Trivy https://gitlab.com/gitlab-org/security-products/dependencies/trivy/-/blob/v0.38.3/pkg/k8s/report/report.go?ref_type=tags#L51
type ConsolidatedReport struct {
	Findings []Resource `json:"Findings"`
}

// Resource Type referenced from Trivy https://gitlab.com/gitlab-org/security-products/dependencies/trivy/-/blob/v0.38.3/pkg/k8s/report/report.go#L58
type Resource struct {
	Namespace string   `json:"Namespace"`
	Kind      string   `json:"Kind"`
	Name      string   `json:"Name"`
	Results   []Result `json:"Results"`
}

// Result Type referenced from Trivy https://gitlab.com/gitlab-org/security-products/dependencies/trivy/-/blob/v0.38.3/pkg/types/report.go#L71
type Result struct {
	Target          string                  `json:"Target"`
	Class           string                  `json:"Class"`
	Type            string                  `json:"Type"`
	Vulnerabilities []DetectedVulnerability `json:"Vulnerabilities"`
}

// DetectedVulnerability Type referenced from Trivy https://gitlab.com/gitlab-org/security-products/dependencies/trivy/-/blob/v0.38.3/pkg/types/vulnerability.go#L9
type DetectedVulnerability struct {
	VulnerabilityID  string `json:"VulnerabilityID"`
	PkgName          string `json:"PkgName"`
	InstalledVersion string `json:"InstalledVersion"`
	FixedVersion     string `json:"FixedVersion"`
	PrimaryURL       string `json:"PrimaryURL"`

	// Embed vulnerability details
	Vulnerability
}

// Vulnerability Type referenced from Trivy-db https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db/-/blob/4bcdf1c414d0/pkg/types/types.go#L132 referenced by Trivy v0.38.3
type Vulnerability struct {
	Title            string     `json:"Title"`
	Description      string     `json:"Description"`
	Severity         string     `json:"Severity"` // Selected from VendorSeverity, depending on a scan target
	References       []string   `json:"References"`
	PublishedDate    *time.Time `json:"PublishedDate"`    // Take from NVD
	LastModifiedDate *time.Time `json:"LastModifiedDate"` // Take from NVD
}

